package com.example.user.playvideo_mvp.activitiy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.user.playvideo_mvp.R;
import com.example.user.playvideo_mvp.model.LoginModel;
import com.example.user.playvideo_mvp.presenter.LoginPresenter;
import com.example.user.playvideo_mvp.view.LoginView;

public class LoginActivity extends AppCompatActivity implements LoginView {


    Button login;
    ProgressBar progressBar;
    LoginPresenter loginPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login=findViewById(R.id.login);
        progressBar=findViewById(R.id.progressBar);

        loginPresenter =new LoginModel(this);
        loginPresenter.setupGoogleClient(this);
        if(loginPresenter.chekUserAllreadySignedIn()){
            Intent intent=new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }





        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginPresenter.loginWithGoogle(LoginActivity.this);
            }
        });
    }

    @Override
    public void onProgressBarShow() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onProgressHide() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoginSuccess() {
        Toast.makeText(this, "Login success", Toast.LENGTH_SHORT).show();
        Intent intent =new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoginFailed() {
        Toast.makeText(this,"login failed",Toast.LENGTH_LONG).show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        loginPresenter.onActivityResult(requestCode, resultCode, data);
    }

}
