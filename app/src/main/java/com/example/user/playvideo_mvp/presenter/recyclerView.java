package com.example.user.playvideo_mvp.presenter;

import android.support.v7.widget.RecyclerView;

import com.example.user.playvideo_mvp.view.MainView;

public class recyclerView implements MainPresenter.recyclerViewPresenter {

    MainView mainView;

    public recyclerView(MainView mainView) {
        this.mainView = mainView;
    }

    @Override
    public void setRecyclerViewAdapter(RecyclerView recyclerView) {
        recyclerView.setAdapter();
    }
}
