package com.example.user.playvideo_mvp.presenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

public interface MainPresenter {

    void getVideoInfo(Context context);

    interface recyclerViewPresenter{
        void setRecyclerViewAdapter(RecyclerView recyclerView);
    }


}
