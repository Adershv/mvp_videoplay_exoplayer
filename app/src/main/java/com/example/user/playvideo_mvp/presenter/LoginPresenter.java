package com.example.user.playvideo_mvp.presenter;

import android.content.Intent;

import com.example.user.playvideo_mvp.activitiy.LoginActivity;

public interface LoginPresenter
{
    void loginWithGoogle(LoginActivity loginView);
    void setupGoogleClient(LoginActivity loginView);
    void onActivityResult(int requestCode, int resultCode, Intent data);
    boolean chekUserAllreadySignedIn();
    void onLogout();

}
