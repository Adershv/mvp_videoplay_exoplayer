package com.example.user.playvideo_mvp.activitiy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.user.playvideo_mvp.R;
import com.example.user.playvideo_mvp.model.LoginModel;
import com.example.user.playvideo_mvp.model.MainModel;
import com.example.user.playvideo_mvp.presenter.LoginPresenter;
import com.example.user.playvideo_mvp.presenter.MainPresenter;
import com.example.user.playvideo_mvp.view.LoginView;
import com.example.user.playvideo_mvp.view.MainView;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements LoginView.logout, MainView {

    Button button;

    LoginPresenter loginPresenter;
    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //button = findViewById(R.id.button1);

        loginPresenter=new LoginModel(this);
        mainPresenter=new MainModel(this);

        mainPresenter.getVideoInfo(this);


//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                loginPresenter.onLogout();
//            }
//        });
    }



    @Override
    public void OnLogout() {
        Toast.makeText(this,"logout",Toast.LENGTH_LONG).show();
        Intent intent=new Intent(MainActivity.this,LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoadVideoFinished(String response) {
        Toast.makeText(this,response,Toast.LENGTH_LONG).show();

    }
}
