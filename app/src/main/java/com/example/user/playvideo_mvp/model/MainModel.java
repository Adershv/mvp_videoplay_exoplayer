package com.example.user.playvideo_mvp.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.user.playvideo_mvp.classes.VolleySingleton;
import com.example.user.playvideo_mvp.presenter.MainPresenter;
import com.example.user.playvideo_mvp.view.MainView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainModel implements MainPresenter {

    List<VideoInfoModel> videoInfoModels =new ArrayList<>();
    VideoInfoModel infoModel =new VideoInfoModel();
    MainView mainView ;
    String  Url="https://interview-e18de.firebaseio.com/media.json?print=pretty";
    private static final String TAG = "MainModel";

    public MainModel(MainView mainView) {
        this.mainView = mainView;
    }



    @Override
    public void getVideoInfo(Context context) {
        StringRequest stringRequest=new StringRequest(Request.Method.GET, Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                                //Log.d(TAG,response);
                        try {
                            JSONArray jsonArray=new JSONArray(response);

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject =jsonArray.getJSONObject(i);
                                infoModel.description=jsonObject.getString("description");
                                infoModel.id=jsonObject.getString("id");
                                infoModel.thumb=jsonObject.getString("thumb");
                                infoModel.url=jsonObject.getString("url");
                                videoInfoModels.add(infoModel);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mainView.onLoadVideoFinished(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    @Override
    public void setRecyclerViewAdapter(RecyclerView recyclerView) {

    }
}
