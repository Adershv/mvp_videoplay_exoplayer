package com.example.user.playvideo_mvp.classes;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {

    private static VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private  static Context context;

    public VolleySingleton(Context context) {
        this.context = context;
        this.requestQueue = getRequestQueue();
    }

    private RequestQueue getRequestQueue() {
        if(requestQueue==null){
            requestQueue= Volley.newRequestQueue(context.getApplicationContext());
            return requestQueue;
        }
        return requestQueue;
    }

    public static synchronized VolleySingleton getInstance(Context context){
        if(volleySingleton==null){
            volleySingleton=new VolleySingleton(context);
        }
        return volleySingleton;
    }

    public  <T> void addToRequestQueue(Request<T> request) {

        requestQueue.add(request);

    }
}
