package com.example.user.playvideo_mvp.view;

public interface LoginView {
    void onProgressBarShow();
    void onProgressHide();
    void onLoginSuccess();
    void onLoginFailed();
    interface logout{
        void OnLogout();
    }




}
