package com.example.user.playvideo_mvp.model;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.user.playvideo_mvp.R;
import com.example.user.playvideo_mvp.activitiy.LoginActivity;
import com.example.user.playvideo_mvp.presenter.LoginPresenter;
import com.example.user.playvideo_mvp.view.LoginView;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginModel implements LoginPresenter{

    int RC_SIGN_IN=100;
    GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;

    LoginView loginView;
    LoginView.logout logout;


    private static final String TAG = "LoginModel";

    public LoginModel(LoginView loginView) {
        this.loginView = loginView;
        mAuth = FirebaseAuth.getInstance();
    }
    public LoginModel(LoginView.logout logout) {
        this.logout = logout;
        mAuth = FirebaseAuth.getInstance();
    }


    @Override
    public void loginWithGoogle(LoginActivity loginView) {

        loginView.onProgressBarShow();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        loginView.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void setupGoogleClient(LoginActivity loginView) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(loginView.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(loginView,gso);



    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.d(TAG, "Google sign in failed", e);

            }
        }
    }

    @Override
    public boolean chekUserAllreadySignedIn() {
        FirebaseUser user = mAuth.getCurrentUser();
        if(user!=null){
            return true;
        }
        else {
            return false;
        }


    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) loginView, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");// Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            loginView.onProgressHide();
                            loginView.onLoginSuccess();
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException()); // If sign in fails, display a message to the user.
                            loginView.onLoginFailed();
                        }

                    }
                });
    }

    @Override
    public void onLogout() {
        mAuth.signOut();
        FirebaseUser user=mAuth.getCurrentUser();
        if(user==null){

            logout.OnLogout();
        }

    }
}
